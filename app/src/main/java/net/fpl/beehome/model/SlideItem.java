package net.fpl.beehome.model;

public class SlideItem {
    private int img;

    public SlideItem(int img) {
        this.img = img;
    }

    public int getImg() {
        return img;
    }

}
